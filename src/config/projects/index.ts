import renorunProd from './renorun-prod';
import { LighthouseRunSettings } from '../../lighthouse/lighthouse-utils';

const projects: Record<string, LighthouseRunSettings[]> = {
  renorunProd
};

export default projects;
